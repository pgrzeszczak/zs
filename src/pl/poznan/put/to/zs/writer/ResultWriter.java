package pl.poznan.put.to.zs.writer;
import java.io.IOException;
import java.io.OutputStream;

import pl.poznan.put.to.zs.algorithm.GeneticZSAlg;
import put.two.to.contest.SolutionAcceptor;

public class ResultWriter extends Thread {
	private boolean shouldStop = false;

	SolutionAcceptor solutionAcceptor;
	int solutionsLimit;
	long timeout;

	public ResultWriter(SolutionAcceptor solutionAcceptor, long timeout, int solutionsLimit) {
		this.solutionAcceptor = solutionAcceptor;
		this.timeout = timeout;
		this.solutionsLimit = solutionsLimit;
	}

	public void stopThread() {
		shouldStop = true;
	}

	@Override
	public void run() {
		while (shouldStop == false) {
			try {
				sleep(timeout/(solutionsLimit-2));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			synchronized (GeneticZSAlg.lock) {
				if (GeneticZSAlg.bestResult != null) {
					OutputStream out = solutionAcceptor
							.newSolutionOutputStream();
					try {
						out.write(GeneticZSAlg.bestResult.getResultToPrint()
								.getBytes());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
}
