package pl.poznan.put.to.zs.structures;

public class Package {
	public String id;
	public int serie;
	public Palette palette;
	public int height;
	public boolean canBeOnOther;
	public boolean canBeUnderOther;
	public Package(String id, int serie, Palette palette, int height, int canBeOnOther, int canBeUnderOther) {
		super();
		this.id = id;
		this.serie = serie;
		this.palette = palette;
		this.height = height;
		this.canBeOnOther = (canBeOnOther == 1);
		this.canBeUnderOther = (canBeUnderOther == 1);
	}
	
	
}
