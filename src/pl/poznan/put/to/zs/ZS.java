package pl.poznan.put.to.zs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.poznan.put.to.zs.reader.SimpleScanner;
import pl.poznan.put.to.zs.structures.Package;
import pl.poznan.put.to.zs.structures.Palette;

public class ZS {

	static int VH, VW;
	static Map<String, Palette> paletts;
	static List<List<Package>> packagesSeries;

	public static void readData(SimpleScanner sc) {
		// rozmiar pierwszej sekcji - tak naprawd� niepotrzebne
		sc.discardCurrentLine();
		// pierwsza sekcja - wymiary samochodu
		VW = sc.getNextAsInt();
		VH = sc.getNextAsInt();

		// rozmiar drugiej sekcji
		int palettCount = sc.getNextAsInt();
		// druga sekcja - palety
		paletts = new TreeMap<String, Palette>();
		for (int i = 0; i < palettCount; i++) {
			Palette p = new Palette(sc.getNextAsString(), sc.getNextAsInt(),
					sc.getNextAsInt());
			paletts.put(p.name, p);
		}

		// rozmiar trzeciej sekcji
		int packagesCount = sc.getNextAsInt();
		// trzecia sekcja - paczki
		List<Package> tmpPackages = new ArrayList<Package>();
		int series = 0;
		for (int i = 0; i < packagesCount; i++) {
			Package p = new Package(sc.getNextAsString(),
					sc.getNextAsInt() - 1, paletts.get(sc.getNextAsString()),
					sc.getNextAsInt(), sc.getNextAsInt(), sc.getNextAsInt());
			tmpPackages.add(p);
			if (p.serie > series) {
				series = p.serie;
			}
		}
		packagesSeries = new ArrayList<List<Package>>();
		for (int i = 0; i <= series; i++) {
			packagesSeries.add(new ArrayList<Package>());
		}

		for (Package p : tmpPackages) {
			packagesSeries.get(p.serie).add(p);
		}
	}

	public static void main(String[] args) throws IOException {
		Calendar calStart = Calendar.getInstance();

		SimpleScanner sc;
		if (args != null && args.length > 0) {
			sc = new SimpleScanner(args[0]);
		} else {
			sc = new SimpleScanner(System.in);
		}

		readData(sc);

		// GeneticZSAlg alg = new GeneticZSAlg(VH, VW, paletts, packagesSeries,
		// calStart);
		//	alg.start();
	}

}
