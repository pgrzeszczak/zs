package pl.poznan.put.to.zs.algorithm;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.PriorityBlockingQueue;

import pl.poznan.put.to.zs.result.Instance;
import pl.poznan.put.to.zs.structures.Package;
import pl.poznan.put.to.zs.structures.Palette;
import pl.poznan.put.to.zs.writer.ResultWriter;
import put.two.to.contest.SolutionAcceptor;

public class GeneticZSAlg {
	int VH, VW;
	Map<String, Palette> paletts;
	List<List<Package>> packagesSeries;
	Calendar calStart;
	SolutionAcceptor solutionAcceptor;
	long timeout;
	int solutionsLimit;

	static public Instance bestResult;
	static public Integer lock = 0;

	public GeneticZSAlg(int vH, int vW, Map<String, Palette> paletts,
			List<List<Package>> packagesSeries, Calendar calStart, long timeout, int solutionsLimit) {
		super();
		VH = vH;
		VW = vW;
		this.paletts = paletts;
		this.packagesSeries = packagesSeries;
		this.calStart = calStart;
		this.timeout = timeout;
		this.solutionsLimit = solutionsLimit;
	}

	public GeneticZSAlg(int vH, int vW, Map<String, Palette> paletts,
			List<List<Package>> packagesSeries, Calendar calStart,
			SolutionAcceptor solutionAcceptor, long timeout, int solutionsLimit) {
		this(vH, vW, paletts, packagesSeries, calStart, timeout, solutionsLimit);
		this.solutionAcceptor = solutionAcceptor;
	}

	public void start() {
		ResultWriter resultWriter = new ResultWriter(solutionAcceptor, timeout, solutionsLimit);
		resultWriter.start();
		Queue<Instance> population = prepareRandomPopulation(packagesSeries, 150);
		int generacja = 0;
		while (true) {
//			System.out.println(generacja++);
//			 System.out.println(population.size());
			Queue<Instance> tmpPopulation = new PriorityQueue<Instance>();
			Instance currentBest = population.poll();
			synchronized (lock) {
				if (currentBest.compareTo(bestResult) > 0) {
					bestResult = currentBest;
				}
			}
			tmpPopulation.add(currentBest);
			for (int i=0; i<25; i++) {
				Instance randomIntance = new Instance(VH, VW,
						createRandomPermutationInSeries(packagesSeries));
				randomIntance.calcGreedySolution();
				tmpPopulation.add(randomIntance);// losowosc
			}
			while (population.size() > 25) {
				Instance crossedInstance = new Instance(VH, VW, crossSeries(
						currentBest.getPackagesSeries(), population.poll()
								.getPackagesSeries()));
				crossedInstance.calcGreedySolution();
				tmpPopulation.add(crossedInstance);
			}
			population = tmpPopulation;
//			System.out.println("ROZMIAR     " + population.size());
			if (Calendar.getInstance().getTimeInMillis()
					- calStart.getTimeInMillis() >= timeout) {
				break;
			}
		}
		resultWriter.stopThread();
		try {
			resultWriter.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private Queue<Instance> prepareRandomPopulation(
			List<List<Package>> initialSeries, int howMany) {
		Queue<Instance> population = new PriorityBlockingQueue<Instance>();
		Instance defaultInstance = new Instance(VH, VW, initialSeries);
		defaultInstance.calcGreedySolution();
		// System.out.println("----------------------------");
		// System.out.println(defaultInstance.getResultToPrint());
		// System.out.println("----------------------------");
		population.add(defaultInstance); // dodaj poczatkowa ulozenie
		for (int i = 1; i < howMany; i++) {
			Instance randomInstace = new Instance(VH, VW,
					createRandomPermutationInSeries(initialSeries));
			randomInstace.calcGreedySolution();
			population.add(randomInstace);
		}
		return population;
	}

	private List<List<Package>> createRandomPermutationInSeries(
			List<List<Package>> initialSeries) {
		// System.out.println("Nowe ulozenie");
		List<List<Package>> randomSeries = new ArrayList<List<Package>>();
		for (List<Package> serie : initialSeries) {
			List<Package> shuffledSerie = new ArrayList<Package>(serie);
			Collections.shuffle(shuffledSerie);
			// System.out.println("\tseria:");
			// for (Package p : shuffledSerie) {
			// System.out.println("\t\t" + p.id);
			// }
			randomSeries.add(shuffledSerie);
		}
		return randomSeries;
	}

	private List<List<Package>> crossSeries(List<List<Package>> series1,
			List<List<Package>> series2) {
		List<List<Package>> crossedSeries = new ArrayList<List<Package>>();
		Random rand = new Random();
		for (int i = 0; i < series1.size(); i++) {
			if (rand.nextDouble() > 0.5) {
				crossedSeries.add(series1.get(i));
			} else {
				crossedSeries.add(series2.get(i));
			}
		}
		return crossedSeries;
	}

}
