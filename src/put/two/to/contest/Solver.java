package put.two.to.contest;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pl.poznan.put.to.zs.algorithm.GeneticZSAlg;
import pl.poznan.put.to.zs.reader.SimpleScanner;
import pl.poznan.put.to.zs.structures.Package;
import pl.poznan.put.to.zs.structures.Palette;

public class Solver {

	static int VH, VW;
	static Map<String, Palette> paletts;
	static List<List<Package>> packagesSeries;

	public static void readData(SimpleScanner sc) {
		// rozmiar pierwszej sekcji - tak naprawd� niepotrzebne
		sc.discardCurrentLine();
		// pierwsza sekcja - wymiary samochodu
		VW = sc.getNextAsInt();
		VH = sc.getNextAsInt();

		// rozmiar drugiej sekcji
		int palettCount = sc.getNextAsInt();
		// druga sekcja - palety
		paletts = new TreeMap<String, Palette>();
		for (int i = 0; i < palettCount; i++) {
			Palette p = new Palette(sc.getNextAsString(), sc.getNextAsInt(),
					sc.getNextAsInt());
			paletts.put(p.name, p);
		}

		// rozmiar trzeciej sekcji
		int packagesCount = sc.getNextAsInt();
		// trzecia sekcja - paczki
		List<Package> tmpPackages = new ArrayList<Package>();
		int series = 0;
		for (int i = 0; i < packagesCount; i++) {
			Package p = new Package(sc.getNextAsString(),
					sc.getNextAsInt() - 1, paletts.get(sc.getNextAsString()),
					sc.getNextAsInt(), sc.getNextAsInt(), sc.getNextAsInt());
			tmpPackages.add(p);
			if (p.serie > series) {
				series = p.serie;
			}
		}
		packagesSeries = new ArrayList<List<Package>>();
		for (int i = 0; i <= series; i++) {
			packagesSeries.add(new ArrayList<Package>());
		}

		for (Package p : tmpPackages) {
			packagesSeries.get(p.serie).add(p);
		}
	}

	public Solver() {

	}

//	public static void main(String[] args) throws IOException {
//		Solver solver = new Solver();
//		InputStream is = new FileInputStream("data/zs105.in");
//		MySolutionAcceptor msa = new MySolutionAcceptor();
//		solver.solve(is, msa, 1000, 20);
//	}

	public void solve(InputStream problemInput,
			SolutionAcceptor solutionAcceptor, long timeout, int solutionsLimit) {
		Calendar calStart = Calendar.getInstance();
		SimpleScanner sc = new SimpleScanner(problemInput);
		readData(sc);
		GeneticZSAlg alg = new GeneticZSAlg(VH, VW, paletts, packagesSeries,
				calStart, solutionAcceptor, timeout, solutionsLimit);
		alg.start();
	}
}
