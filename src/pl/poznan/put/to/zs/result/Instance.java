package pl.poznan.put.to.zs.result;

import java.util.List;

import pl.poznan.put.to.zs.structures.Package;

public class Instance implements Comparable<Instance> {
	int VH, VW;
	List<List<Package>> packagesSeries;
	int width;
	int area;
	String localResult;

	static int ile = 0;

	public Instance(int vH, int vW, List<List<Package>> packagesSeries) {
		super();
		VH = vH;
		VW = vW;
		this.packagesSeries = packagesSeries;
	}

	public String getResultToPrint() {
		return localResult;
	}

	public int currentWidth(Package paczka, int rotation) {
		return paczka.palette.size[(rotation + 1) % 2];
	}

	public int currentLength(Package paczka, int rotation) {
		return paczka.palette.size[rotation];
	}

	public boolean canPutFirstOnSecond(Package first, int rot1, Package second,
			int rot2) {
		if (!first.canBeOnOther)
			return false;
		if (!second.canBeUnderOther)
			return false;
		if (currentWidth(first, rot1) > currentWidth(second, rot2))
			return false;
		if (currentLength(first, rot1) > currentLength(second, rot2))
			return false;
		return true;
	}

	public void calcGreedySolution() {
//		ile++;
//		if (ile % 1000 == 0)
//			System.out.println("Liczę Greeedy po raz " + ile);
		String myResult = "";

		int wysokosci[] = new int[VW];
		int glebokosci[] = new int[VW];
		int rotejszony[] = new int[VW];
		Package paczki[] = new Package[VW];
		int pole = 0;
		int liczbaPaczek = 0;
		int koncowaGlebokosc = 0;
		for (int lol = 0; lol < 1; lol++) {

			for (List<Package> listaPaczek : packagesSeries) {
				for (Package paczka : listaPaczek) {
					liczbaPaczek++;
					int bestGlebokosc = -1;
					int bestX = 0;
					int bestRotation = 0;
					int bestOffset = 0;
					boolean polozylemPaczke = false;

					// Sprawdzamy najpierw wszystkie możliwości bez rotacji,
					// potem wszystkie możliwości z rotacją..
					for (int rotation = 0; rotation <= 1; rotation++) {
						// I próbujemy najpierw najbardziej z lewej, po kroczku
						// przesuwając się w prawo..
						for (int x = 0; x < VW; x++) {
							// Na początku próbujemy położyć w tym miejscu na
							// paczce, o ile jakaś się znajduje, i zmieści się,
							// i mamy korytarz..
							if (paczki[x] != null
									&& x + currentWidth(paczka, rotation) <= VW
									&& paczki[x
											+ currentWidth(paczka, rotation)
											- 1] != null
									&& canPutFirstOnSecond(paczka, rotation,
											paczki[x], rotejszony[x])
									&& wysokosci[x] + paczka.height <= VH
									&& paczki[x].id.equals(paczki[x
											+ currentWidth(paczka, rotation)
											- 1].id)) {

								// Udało się. Kładziemy paczkę na paczce..
								// Aktualizujemy wszystkie wysokości,
								// głębokośći, takie tam..
								String oldPaczkaId = paczki[x].id;
								int oldGlebokosc = glebokosci[x]
										- currentLength(paczka, rotejszony[x]);

								// odPrawejX.. Ogólnie kłade paczki od lewej
								// strony.. Ale jak paczkę kładę na paczkę,
								// kłądą ją jak najbardziej w prawo.. (po lewo
								// szybciej się zapcha korytarz, wolę mieć po
								// prawo możliwość kładzenia dłużej).. tu
								// wyliczamy, w którym miejscu faktycznie packzę
								// położymy..
								int odPrawejX = currentWidth(paczki[x],
										rotejszony[x])
										- currentWidth(paczka, rotation);
								int tmpOffset = -1;
								for (int xx = 0; xx < VW; xx++) {

									if (paczki[xx] != null
											&& paczki[xx].id
													.equals(oldPaczkaId)) {
										if (tmpOffset == -1)
											tmpOffset = xx;
										paczki[xx] = paczka;
										wysokosci[xx] += paczka.height;
									}
								}
								odPrawejX += tmpOffset;
								polozylemPaczke = true;
								myResult += paczka.id + "\t" + rotation + "\t"
										+ odPrawejX + "\t" + oldGlebokosc
										+ "\n";

							} else {
								// Na paczce nie da rady.. patrzymy więc, jaki
								// wynik byśmy osiągneli, kładąc paczkę w tym
								// miejscu na podłodze..
								if (x + currentWidth(paczka, rotation) > VW)
									continue;
								// ileWysunac.. bo tutaj paczka przed którą
								// chcemy położyć może wystawać delikatnie, a po
								// prawej nagle wystawać mocno.. musimy się
								// dotosować do tej po prawej, jeżeli nasza
								// paczka wystarczająco szeroka..
								int ileWysunac = 0;
								for (int xx = 0; xx < currentWidth(paczka,
										rotation); xx++) {
									if (glebokosci[x + xx] > glebokosci[x]) {
										ileWysunac = Math.max(ileWysunac,
												glebokosci[x + xx]
														- glebokosci[x]);
									}
								}

								// Jak nowy wynik jest lepszy od poprzedniego,
								// to zapamiętujemy go..
								if (bestGlebokosc == -1
										|| glebokosci[x]
												+ currentLength(paczka,
														rotation) < bestGlebokosc) {
									bestGlebokosc = glebokosci[x]
											+ currentLength(paczka, rotation);

									bestX = x;
									bestRotation = rotation;
									bestOffset = ileWysunac;
								}

							}
							if (polozylemPaczke)
								break;
						}
						if (polozylemPaczke)
							break;
					}
					// Jeśli paczkę położyłem na paczce, to my job is done. W
					// przeciwnym wypadku, muszę uaktualnić tablice, dopisać coś
					// do wyjściowego wyniku..
					if (!polozylemPaczke) {
						int oldGlebokosc = glebokosci[bestX] + bestOffset;
						int glebokosc = glebokosci[bestX]
								+ currentLength(paczka, bestRotation)
								+ bestOffset;
						for (int x = 0; x < currentWidth(paczka, bestRotation); x++) {
							paczki[bestX + x] = paczka;
							wysokosci[bestX + x] = paczka.height;
							glebokosci[bestX + x] = glebokosc;

						}
						if (glebokosc > koncowaGlebokosc)
							koncowaGlebokosc = glebokosc;
						pole += paczka.palette.size[0] * paczka.palette.size[1];
						myResult += paczka.id + "\t" + bestRotation + "\t"
								+ bestX + "\t" + oldGlebokosc + "\n";
					}

				}

			}
			width = koncowaGlebokosc;
			area = pole;
			localResult = "1\n" + koncowaGlebokosc + "\t" + pole + "\n";
			localResult += liczbaPaczek + "\n";
			localResult += myResult;

		}
	}

	@Override
	public int compareTo(Instance o) {
		if (o == null)
			return 1;
		if (width < o.width)
			return -1;
		if (width > o.width)
			return 1;
		if (area < o.area)
			return -1;
		if (area > o.area)
			return 1;
		return 0;
	}

	public int getWidth() {
		return width;
	}

	public int getArea() {
		return area;
	}

	public List<List<Package>> getPackagesSeries() {
		return packagesSeries;
	}
}
