package pl.poznan.put.to.zs.reader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class SimpleScanner {
	private BufferedReader bi;
	private int currentPos;
	private String[] splittedCurrentLine;
	
	public SimpleScanner(InputStream inputStream) {
		bi = new BufferedReader(new InputStreamReader(inputStream));
		discardCurrentLine(); //initialize first
	}
	
	public SimpleScanner(String filename) throws FileNotFoundException {
		this(new FileInputStream(filename));
	}
	
	public void discardCurrentLine() {
		try {
			splittedCurrentLine = bi.readLine().split("\\s+");
		} catch (IOException e) {
			e.printStackTrace();
		}
		currentPos = 0;
	}

	public boolean hasNext() {
		prepareNextLineIfNeeded();
		return splittedCurrentLine != null;
	}
	
	public String getNextAsString() {
		String token = getNextToken();
		return (token == null) ? "" : token;
	}
	
	public int getNextAsInt() {
		String token = getNextToken();
		return (token == null) ? 0 : Integer.parseInt(token);
	}
	
	private String getNextToken() {
		if (!hasNext()) return null;
		return splittedCurrentLine[currentPos++];
	}
	
	private void prepareNextLineIfNeeded() {
		if (splittedCurrentLine == null || currentPos >= splittedCurrentLine.length) {
			String line;
			try {
				line = bi.readLine();
				splittedCurrentLine = line.split("\\s+");
			} catch (IOException e) {
				splittedCurrentLine = null;
			}
			currentPos = 0;
		}
	}
}
