package pl.poznan.put.to.zs.structures;

public class Palette {
	public String name;
	public int[] size;
	
	public Palette(String name, int dim1, int dim2) {
		this.name = name;
		size = new int[] {dim1, dim2};
	}
}
